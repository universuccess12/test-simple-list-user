const avatarLink = (id) => `https://i.pravatar.cc/300?u=${id}`
export {
    avatarLink
}